#### Test support for Python macros

* Open new document Writer
* Select menu: **Tools -> Macro -> Run macros...**
* Select Library: **LibreOffice Macros** -> **HelloWorld**
* Select macro **HelloWorldPython**
* Click in command button **Run**
![Run Macros](img/img_intro_01.png)

<BR>
* If you see like next image, your system can run Python macros.
![Test Python Macros](img/img_intro_02.png)

<BR>
#### Where save my macros?

* GNU/Linux
    * For all users:
        * `/usr/lib/libreoffice/share/Scripts/python/`
    * For current user:
        * `~/.config/libreoffice/4/user/Scripts/python`

<BR>

<div class="alert-box warning"><span>warning: </span>
    The following procedure may leave the file broken, always create a backup file.
</div>

* Inside a document

    * Save new document, for example, a document Writer and close.
    * Copy file and rename extension to **zip**.

            .
            ├── MyMacros.odt
            └── MyMacros.zip

    * Open file **zip** with your prefer software for this type files.
    * Into root, create folders `Scripts/python`
    * Copy file with macros inside this folder.
        ![ZIP File](img/img_intro_03.png)
    * In root folder, edit file **manifest.xml** inside folder **META-INF**
        ![ZIP File](img/img_intro_04.png)
    * Add this lines, just before last tag `</manifest:manifest>`


                <manifest:file-entry manifest:media-type="" manifest:full-path="Scripts/python/mymacros.py"/>
                <manifest:file-entry manifest:media-type="application/binary" manifest:full-path="Scripts/python/"/>
                <manifest:file-entry manifest:media-type="application/binary" manifest:full-path="Scripts/"/>
            </manifest:manifest>

    * Save and close.
    * Delete origin file ODT and rename file **zip** to **odt**.
    * Open file and activate macros.
    * Select menu Tools -> Macro -> Run macros...
    * If you see the library (*mymacros*) and the macro, congratulations!
        ![Macros inside document](img/img_intro_05.png)


After we'll see how to automatize this.

<BR>
#### Our first macro
<BR>
<div class="alert-box warning"><span>warning: </span>
    Python is very strict are its syntax, caution when write or copy and paste.
</div>
<BR>

* Inside folder macros for current user, remember:
    * GNU/Linux = `~/.config/libreoffice/4/user/Scripts/python`
* Create file **mymacros.py**, this file is our first macro library.
* With your favorite plain text editor, write next macro, yes, you can copy and paste.

```python
import uno

def my_first_macro_writer():
    doc = XSCRIPTCONTEXT.getDocument()
    text = doc.getText()
    text.setString('Hello World Writer, with Python')
    return
```

* Don't worry, we'll explain every line later.
* Open new document Writer
* Select menu: **Tools -> Macro -> Run macros...**
* Select Library: **My Macros** -> **mymacros**
* Select macro **my_first_macro_writer**
* Click in command button **Run**

<BR>

* Now, the same but with Calc.
* In the same file, you write.

```python
import uno

def my_first_macro_calc():
    doc = XSCRIPTCONTEXT.getDocument()
    cell = doc.Sheets[0]['A1']
    cell.setString('Hello World Calc, with Python')
    return
```

* Of course, now open document Calc and execute.
